import json
import urllib.request
import datetime as dt
from datetime import timedelta

class Time:
    def __init__(self):
        self.start = 0
        self.stop = dt.datetime.now()
        self.startDate = lambda delta: timedelta(days=delta)
        self.dateStack = []

    def toString(self):
        """
        Converts start and stop so to get data from API
        """
        self.start = str(self.start)[:10]
        self.stop = str(self.stop)[:10]

    def initialise(self, period):
        """
        w - week
        m - month
        y - year
        t - 3 years
        """
        if period == 'w':
            self.start = self.stop - self.startDate(7)
        elif period == 'm':
            self.start = self.stop - self.startDate(30)
        elif period == 'y':
            self.start = self.stop - self.startDate(365)
        elif period == 't':
            for _ in range(3):
                self.start = self.stop - self.startDate(365)
                pair = [str(self.start)[:10], str(self.stop)[:10]]
                self.dateStack.append(pair)
                self.stop = self.start - self.startDate(1)
        else:
            print('Possible error, period unaviable')

        self.toString()

class GetData(Time):
    def __init__(self, period='m'):
        super().__init__()
        self.period = period
        self.currencyTypes = ['aud', 'usd', 'eur', 'gbp', 'nok']
        self.data = []
        self.dates = []
        self.stackDateFlag = 1

    def getData(self, currency):
        final = []
        src = f'https://api.nbp.pl/api/exchangerates/rates/a/{currency}/{self.start}/{self.stop}/?format=json'
        isEmptyFlag = len(self.dates)
    
        with urllib.request.urlopen(src) as url:
            data = json.loads(url.read())
            for datum in data['rates']:
                final.append(datum['mid'])
                if isEmptyFlag == 0:
                    self.dates.append(datum['effectiveDate'])

        self.data.append(final)

    def getMultiData(self, currency):
        tmpDateStack = self.dateStack[::-1]
        temp = []
        for year in tmpDateStack:
            src = f'https://api.nbp.pl/api/exchangerates/rates/a/{currency}/{year[0]}/{year[1]}/?format=json'

            with urllib.request.urlopen(src) as url:
                data = json.loads(url.read())
                for datum in data['rates']:
                    temp.append(datum['mid'])
                    if self.stackDateFlag:
                        self.dates.append(datum['effectiveDate'])

        self.data.append(temp)
        self.stackDateFlag = 0

    def run(self):     
        self.initialise(self.period)

        if not len(self.dateStack):
            for ctype in self.currencyTypes:
                self.getData(ctype)
        else:
            for ctype in self.currencyTypes:
                self.getMultiData(ctype)
