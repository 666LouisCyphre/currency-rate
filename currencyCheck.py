from getModule import GetData
from guiModule import startx

import matplotlib.pyplot as plt
from matplotlib.widgets import Button
from matplotlib.widgets import CheckButtons

rates = GetData(period=startx())
rates.run()

rate = rates.data
period = rates.dates


plt.ion()

fig, ax = plt.subplots()
fig.canvas.set_window_title('Currency Rates')
fig.autofmt_xdate()
aud, = ax.plot(period, rate[0], label='AUD')
usd, = ax.plot(period, rate[1], label='USD')
eur, = ax.plot(period, rate[2], label='EUR')
gbp, = ax.plot(period, rate[3], label='GBP')
nok, = ax.plot(period, rate[4], label='NOK')

plt.subplots_adjust(left=0.25, bottom=0.16, right=0.95, top=0.95)

lines = [aud, usd, eur, gbp, nok]
labels = ['AUD', 'USD', 'EUR', 'GBP', 'NOK']
activated = [True, True, True, True, True]
axCheckBox = plt.axes([0.01, 0.4, 0.1, 0.20])
chxbox = CheckButtons(axCheckBox, labels, activated)

graphs = [True, True, True, True, True]

def mini():
    minimal = [min(i) for i in rate]
    indexes = [i for i in range(len(graphs)) if graphs[i] == False] #return indexes of False
    minimal = [minimal[i] for i in range(len(minimal)) if not i in indexes]
    value = min(minimal)
    return value - 0.07 #add margin

def maxi():
    maximal = [max(i) for i in rate]
    indexes = [i for i in range(len(graphs)) if graphs[i] == False]
    maximal = [maximal[i] for i in range(len(maximal)) if not i in indexes]
    value = max(maximal)
    return value + 0.07 #add margin

def limit():
    if(True in graphs):
        minimal = mini()
        maximal = maxi()
        global ax
        ax.set_ylim([minimal, maximal])

def set_visible(label):
    index = labels.index(label)
    lines[index].set_visible(not lines[index].get_visible())

    global graphs
    graphs[index] = not graphs[index]
    limit()
    plt.draw()

chxbox.on_clicked(set_visible)

ax.legend()
ax.grid(True)
ax.set_title("Currency Rates")
ax.set_ylabel("Rates [PLN]")

if rates.period == 'y':
    n = 30  # Keeps every 7th label
    [l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]
elif rates.period == 't':
    n = 90  # Keeps every 7th label
    [l.set_visible(False) for (i,l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]

plt.ioff()
mng = plt.get_current_fig_manager()
mng.window.state('zoomed')
plt.show()



