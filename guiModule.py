import tkinter as tk

root = tk.Tk()

class GUI(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.period = ''
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        btnWeek = tk.Button(self, text="Week", command = lambda p='w': self.ret(p))
        btnWeek.pack()

        btnMonth = tk.Button(self, text="Month", command = lambda p='m': self.ret(p))
        btnMonth.pack()

        btnYear = tk.Button(self, text="Year", command = lambda p='y': self.ret(p))
        btnYear.pack()

        btnThreeYears = tk.Button(self, text="Three Years", command = lambda p='t': self.ret(p))
        btnThreeYears.pack()

    def ret(self, p):
        self.period = p
        root.destroy()

def startx():
    app = GUI(master=root)
    app.mainloop()
    return app.period
